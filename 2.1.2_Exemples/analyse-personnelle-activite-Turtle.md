# Objectifs

- Réinvestir des notions d'algorithmique vues lors du cycle 4, avec le logiciel Scratch.
* Il s'agit essentiellement de reproduire des motifs géométriques.
* On peut retrouver ce genre d'exercices dans l'épreuve de mathématiques du Brevet des collèges.
- Introduire le langage Python, avec un résultat directement visible par l'élève, après execution du code.
* Opérations de débogage induites par la figure obtenue.

# Pré-requis à cette activité

- Etre familiarisé avec l'interpréteur Python
- Connaitre certaines instructions en algorithmique (boucles conditionnelles, itérations)
- Avoir déjà réalisé du codage en Python (attention aux erreurs de syntaxe, indentations, ...)
- Les élèves doivent savoir mettre un stylo "en position d'écriture" ou notions
- Connaitre les propriétés géométriques simples des figures géométriques mobilisées

# Durée de l'activité

- Sur plusieurs séances (au moins 2 séances d'une heure)

# Exercices cibles

L'objectif est de réaliser tous les exercices du niveau *.
Ils mobilisent les notions principales du cours.
Il n'est pas nécessaire que tous les élèves aient réalisés tous les exercices.
Les meilleurs et plus rapides pourront aller plus loin.

# Description du déroulement de l'activité

Ce document, avec le nombre d'exercices proposés, sous-entend qu'il s'agit d'un défi à relever par les élèves.
Le document est distribué à tous les élèves, ils travaillent individuellement.
La séance est construite de façon à proposer ce document sous la forme d'un défi à réaliser.
Les exercices sont à faire dans l'ordre proposé, car ils sont de difficulté croissante.

# Anticipation des difficultés des élèves

- Le seul exemple proposé est insuffisant pour comprendre la mécanique relative aux constructions des différents polygones réguliers.
Il faudra éventuellement faire un exemple corrigé au tableau avec l'aide des élèves afin de dégager une méthode.
Cette méthodologie sera inscrite au tableau, de manière ostensible (faire apparaitre le "motifs" qui se répétent, les comptabiliser, rédiger le code en langage naturel, etc)
- Problème mathématique avec la notion d'angle. Erreurs entre gauche et droite.
- Problème pour faire en sorte que la tortue ne trace plus de segment.

# Gestion de l'hétérogénéïté

Cependant, ce ne serait pas la mise en oeuvre que je retiendrais.
Je préferais en donner moins.
Nous aurions lors d'une première séance, réalisés entièrement les exercices du niveau * seulement.
Lors d'une seconde séance, j'aurais proposé un travail de groupe, en proposant cette fois-ci une selection d'exercices de niveau ** et ***
Il s'agirait de travailler en équipe afin que les élèves d'un même groupe puissent comparer leurs solutions.
Cette séance sera ludifiée, de façon à récompenser l'équipe ayant avancée le plus rapidement dans les défis proposés.
Cela permettra de gérer l'hétérogénéité de la classe et répondre aux difficultés des élèves.
L'enseignant pourra circuler dans les rangs et venir en aide aux groupes en difficulté.
Si l'enseignant souhaite faire travailler les élèves en toute autonomie, il serait possible d'accompagner chacun de ces exercices d'un QR-Code à flasher, donnant une indication en cas de blocage.

